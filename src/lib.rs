#[macro_use]
extern crate lazy_static;
extern crate libc;
extern crate sdl2;
extern crate time;

#[macro_use]
pub mod util;

pub mod apu;
pub mod audio;
#[macro_use]
pub mod cpu;
pub mod disasm;
pub mod mem;
pub mod nsf;
pub mod resampler;

use apu::Apu;
use cpu::Cpu;
use mem::MemMap;
use nsf::Nsf;
use util::Save;

//se sdl2::EventPump;

use std::cell::RefCell;
use std::fs::File;
use std::path::Path;
use std::rc::Rc;
use std::io::Write;
use std::thread::sleep_ms;

pub struct Player {
    cpu: Cpu<MemMap>,
    mute: bool,
    current_track: u8,
    init_address: u16,
    play_address: u16,
    play_speed: u64
}

impl Player {
    /// Creates a new player
    pub fn new(nsf: Nsf) -> Player {
        //let nsf = Box::new(nsf);
        println!("Loaded NSF: {}", nsf.header);

        let sdl = sdl2::init().unwrap();
        let audio = sdl.audio().unwrap();
        let audio_buffer = audio::open(&audio);

        let apu = Apu::new(audio_buffer);
        println!("Initialize memory...");
        //Initialize ram
        //TODO: bank switching
        let mut memmap = MemMap::new(apu, nsf.data.clone(), nsf.header.load_address);
        println!("Initialize data, #{} at ${:x}...", nsf.header.starting_song, nsf.header.init_address);
        let mut cpu = Cpu::new(memmap);
        cpu.reset();

        Player {
            cpu: cpu,
            mute: false,
            current_track: nsf.header.starting_song,
            init_address: nsf.header.init_address,
            play_address: nsf.header.play_address,
            play_speed: nsf.header.play_speed as u64,
        }
    }

    /// Starts the emulator main loop. Returns when the user presses escape or the window is
    /// closed.
    pub fn start(&mut self, track: u8) {
        let mut last_time = time::precise_time_s();
        let mut frame: u64 = 0;
        self.current_track = track;

        // TODO: Add a flag to not reset for nestest.log
        self.cpu.init_regs(track - 1, 0/*nsf.header.is_pal*/);
        self.cpu.call(self.init_address, true);
        println!("Start playing, #{} at ${:x}...", self.current_track, self.play_address);

        'main: loop {

            self.cpu.call(self.play_address, true);
            for i in 0 .. 5 {
                frame += 1;
                self.cpu.mem.apu.step(frame * 6000);
                self.cpu.mem.apu.play_channels(self.mute);
            }
        }

        audio::close();
    }
}
