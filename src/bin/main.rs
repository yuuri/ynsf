extern crate ynsf;

use ynsf::nsf::Nsf;
use ynsf::Player;

use std::env;
use std::path::Path;
use std::fs::File;

fn main() {
    let mut args = env::args();
    args.next();
    let path = args.next().unwrap();
    let track: u8 = args.next().unwrap().parse().unwrap();
    let nsf = Nsf::load(&mut File::open(&Path::new(&path)).unwrap()).unwrap();

    let mut player = Player::new(nsf);
    player.start(track);
}
