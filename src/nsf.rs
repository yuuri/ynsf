//! Contains iNES ROM loading code.

//
// Author: Patrick Walton
//

use util;

use std::io::{self, Read};
use std::fmt;
use std::vec::Vec;
use std::str;

//#[repr(C)]
//#[packed]
#[derive(Clone)]
pub struct NsfHeader {
    magic: [u8; 6], //including version
    pub total_songs: u8,
    pub starting_song: u8,
    pub load_address: u16,
    pub init_address: u16,
    pub play_address: u16,
    raw_name: [u8; 32],
    raw_artist: [u8; 32],
    raw_copyright: [u8; 32],
    pub play_speed: u16,
    bankswitch: [u8; 8],
    play_speed_pal: u16, //currently unused
    pub is_pal: u8,
    extra_sound: u8,
    expansion: u32, //currently unused
}

impl fmt::Display for NsfHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{} by {}\nTotal: {}, start: {}\nPlay rate:{}",
            str::from_utf8(&self.raw_name[..]).unwrap(),
            str::from_utf8(&self.raw_artist[..]).unwrap(),
            self.total_songs,
            self.starting_song,
            self.play_speed,
        )
    }
}


#[derive(Debug)]
pub enum NsfLoadError {
    /// IO error while reading the ROM image
    IoError(io::Error),
    /// The ROM image has an invalid format
    FormatError,
}

impl From<io::Error> for NsfLoadError {
    fn from(err: io::Error) -> Self {
        NsfLoadError::IoError(err)
    }
}

/// A ROM image
pub struct Nsf {
    pub header: NsfHeader,
    pub data: Vec<u8>,
}

impl Nsf {
    pub fn load(r: &mut Read) -> Result<Nsf, NsfLoadError> {
        let mut buf = [ 0u8; 0x80 ];
        try!(util::read_to_buf(&mut buf, r));

        let ptr: *const u8 = buf.as_ptr();
        let ptr: *const NsfHeader = ptr as *const NsfHeader;
        let header: &NsfHeader = unsafe { &*ptr };

        if header.magic != *b"NESM\x1a\x01" { return Err(NsfLoadError::FormatError); }

        let mut data = Vec::new();
        try!(r.read_to_end(&mut data));

        Ok(Nsf {
            header: header.clone(),
            data: data
        })
    }
}
